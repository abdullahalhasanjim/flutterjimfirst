import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jimfirst/practice/end_drawer.dart';
import 'package:flip_card/flip_card.dart';
import 'country_map.dart';

class Country extends StatelessWidget {
  final Map country;
  Country(this.country);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(country['name']),
        backgroundColor: Colors.pink,
      ),
      endDrawer: EndDrawer(),
      body: Container(
        padding: EdgeInsets.all(8),
        child: GridView(
          gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          children: <Widget>[
            FlipCard(
              direction: FlipDirection.HORIZONTAL,
              front: CountryCard(name: 'Capital'),
              back: DetalCard(capital: country['capital'],color: Colors.deepOrange),
            ),
            FlipCard(
              direction: FlipDirection.HORIZONTAL,
              front: CountryCard(name: 'Population'),
              back: DetalCard(capital: country['population'].toString(),color: Colors.deepPurple),
            ),
            FlipCard(
              direction: FlipDirection.HORIZONTAL,
              front: CountryCard(name: 'Flag'),
              back: Card(
                elevation: 10,
                child: Center(
                  child : SvgPicture.network(country['flag'],width : 200)
                ),
              ),
            ),
            FlipCard(
              direction: FlipDirection.HORIZONTAL,
              front: CountryCard(name: 'Currency'),
              back: DetalCard(capital: country['currencies'][0]['name'],color: Colors.red),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => CountryMap(countryName: country['name'],latLang : country['latlng'] ),
                  ),
                );
              },
              child : Card(
                elevation: 10,
                child: Center(
                    child: Text("Go On Map",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)
                            )
                          )
                       ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => PakundiaMap(),
                  ),
                );
              },
              child : Card(
                elevation: 10,
                child: Center(
                    child: Text("Go to Pakundia",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)
                            )
                          )
                       ),
            )
          ],
        ),
      ),
    );
  }
}

class CountryCard extends StatelessWidget {
  final String name;
  const CountryCard({Key key, this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 10,
        child: Center(
            child: Text(name,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))));
  }
}

class DetalCard extends StatelessWidget {
  final String capital;
  final MaterialColor color;
  const DetalCard({Key key, this.capital, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        color: color,
        elevation: 10,
        child: Center(
            child: Text(capital,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold))));
  }
}

import 'package:flutter/material.dart';
import 'package:jimfirst/practice/end_drawer.dart';
import 'blood/single_blood_group_list.dart';

class BloodGroup extends StatelessWidget {
    final String longTextBlood = "বিশ্ব স্বাস্থ্য সংস্থার পরিসংখ্যান অনুযায়ী, প্রতিবছর বিশ্বের ৯ কোটি ২০ লাখ মানুষ রক্ত দিয়ে থাকে। তবে উন্নত বিশ্বে স্বেচ্ছা রক্তদানের হার প্রতি এক হাজারে ৪০ জন হলেও উন্নয়নশীল বিশ্বে প্রতি এক হাজারে ৪ জনেরও কম। বিশ্ব স্বাস্থ্য সংস্থার লক্ষ্য ২০২০ সালের মধ্যে স্বেচ্ছায় রক্তদানের মাধ্যমে চাহিদার শতভাগ রক্তের সরবরাহ নিশ্চিত করা। এই লক্ষ্যে প্রতিবছরের ১৪ জুন বিশ্ব রক্তদাতা দিবস পালন হয়ে আসছে। মূলত যারা মানুষের জীবন বাঁচাতে স্বেচ্ছায় ও বিনামূল্যে রক্তদান করেন তাদের দানের মূল্যায়ন, স্বীকৃতি দিতে সেইসঙ্গে সাধারণ মানুষকে রক্তদানে উৎসাহিত করার লক্ষ্যে দিবসটি পালন করা হয়। রক্ত দেয়ার পর লোহিত রক্তকণিকার মাত্রা স্বাভাবিক অবস্থায় ফিরে যেতে অন্তত ";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("রক্তের গ্রুপ"),
        backgroundColor: Colors.pink,
      ),
      endDrawer: EndDrawer(),
      body: Container(
        // child : BloodGroupCard(),
        child: Column(
          children : <Widget> [
            Card(
              elevation: 10,
              child: BloodGroupCard(),),
            Card(
              elevation: 10,
              child : Column (
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.all(10),
                    child: SingleChildScrollView(child: Text(longTextBlood + longTextBlood),),
                    ),
                  ],
      ),
            )
          ]
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          // Add your onPressed code here!
        },
        label: Text('join'),
        icon: Icon(Icons.add),
        backgroundColor: Colors.pink,
      ),
      );
  }
}

class BloodGroupCard extends StatelessWidget {
  final List<dynamic> teams = [
    {'name': "A+"},
    {'name': "A-"},
    {'name': "B+"},
    {'name': "B-"},
    {'name': "O+"},
    {'name': "O-"},
    {'name': "AB+"},
    {'name': "AB-"},
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 185,
      child: GridView.count(
        crossAxisCount: 4,
        children: List.generate(teams.length, (index) {
          return Center(
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) =>
                        SingleBloodGroupList(bloodGroupIndex: index)));
              },
              child: Padding(
                padding: EdgeInsets.all(5),
                child: BackCard(name: teams[index]['name'], color: Colors.red),
              ),
            ),
          );
        }),
      ),
    );
  }
}

class BackCard extends StatelessWidget {
  final String name;
  final MaterialColor color;
  const BackCard({Key key, this.name, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      elevation: 10,
      child: Center(
        child: Text(
          name,
          style: TextStyle(
              color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'news_details.dart';
import 'blood_group.dart';
import '../product.dart';

class HomeDashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //  backgroundColor: Colors.blueAccent[700],
        backgroundColor: Colors.white,
        body: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(2),
              child: Row(children: <Widget>[
                Container(
                    width: 175,
                    height: 140,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => BloodGroup()));
                      },
                      child: Card(
                        semanticContainer: true,
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        child: Image.network(
                          'https://challengepost-s3-challengepost.netdna-ssl.com/photos/production/software_photos/000/545/093/datas/original.jpg',
                          fit: BoxFit.fill,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        elevation: 5,
                        margin: EdgeInsets.all(10),
                      ),
                    )),
                Container(
                  width: 175,
                  height: 140,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ProductPage()));
                    },
                    child: Card(
                      semanticContainer: true,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      child: Image.network(
                        'https://www.meaningfulwomen.com/wp-content/uploads/The-Most-Popular-Promotional-Products-of-2018.jpg',
                        fit: BoxFit.fill,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 5,
                      margin: EdgeInsets.all(10),
                    ),
                  ),
                )
              ]),
            ),
            Padding(
              padding: EdgeInsets.all(2),
              child: Row(
                children: <Widget>[
                  Container(
                      width: 117,
                      height: 100,
                      child: GestureDetector(
                        onTap: () {
                          print('This is the tap of Blood');
                        },
                        child: Card(
                          semanticContainer: true,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Image.network(
                            'https://vitagene.com/wp-content/uploads/2018/07/hearthealth1.jpg',
                            fit: BoxFit.fill,
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 5,
                          margin: EdgeInsets.all(10),
                        ),
                      )),
                  Container(
                    width: 117,
                    height: 100,
                    child: GestureDetector(
                      onTap: () {
                        print('This is the tap of Products');
                      },
                      child: Card(
                        semanticContainer: true,
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        child: Image.network(
                          'https://1.bp.blogspot.com/_3zqObIZvPpk/SrpY_sSW2SI/AAAAAAAAAKs/edqtKXltJUI/s400/young+children+working+in+group.bmp',
                          fit: BoxFit.fill,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        elevation: 5,
                        margin: EdgeInsets.all(10),
                      ),
                    ),
                  ),
                  Container(
                    width: 117,
                    height: 100,
                    child: GestureDetector(
                      onTap: () {
                        print('This is the tap of Products');
                      },
                      child: Card(
                        semanticContainer: true,
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        child: Image.network(
                          'https://thumbs.dreamstime.com/b/more-added-additional-word-extra-increase-d-illustration-83854912.jpg',
                          fit: BoxFit.fill,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        elevation: 5,
                        margin: EdgeInsets.all(10),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
                padding: EdgeInsets.all(2),
                // child : SingleChildScrollView(
                //   child : (
                //     Container(
                //       height: 400,
                //       child: Column(

                //     children: <Widget>[
                //       ListTile(
                //         onTap: () {

                //         },
                //         leading : CircleAvatar(
                //             radius: 30.0,
                //             backgroundImage:
                //                 NetworkImage('https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                //             backgroundColor: Colors.transparent,
                //           ),
                //         title: Text("There is a girl which is hot ..."),
                //         subtitle: Text("She is a beautiful girl, her name is sadia priti, and she is super ..."),
                //       ),
                //       ListTile(
                //         onTap: () {

                //         },
                //         leading : CircleAvatar(
                //             radius: 30.0,
                //             backgroundImage:
                //                 NetworkImage('https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                //             backgroundColor: Colors.transparent,
                //           ),
                //         title: Text("There is a girl which is hot ..."),
                //         subtitle: Text("She is a beautiful girl, her name is sadia priti, and she is super ..."),
                //       ),

                //       ListTile(
                //         onTap: () {

                //         },
                //         leading : CircleAvatar(
                //             radius: 30.0,
                //             backgroundImage:
                //                 NetworkImage('https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                //             backgroundColor: Colors.transparent,
                //           ),
                //         title: Text("There is a girl which is hot ..."),
                //         subtitle: Text("She is a beautiful girl, her name is sadia priti, and she is super ..."),
                //       ),
                //       ListTile(
                //         onTap: () {

                //         },
                //         leading : CircleAvatar(
                //             radius: 30.0,
                //             backgroundImage:
                //                 NetworkImage('https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                //             backgroundColor: Colors.transparent,
                //           ),
                //         title: Text("There is a girl which is hot ..."),
                //         subtitle: Text("She is a beautiful girl, her name is sadia priti, and she is super ..."),
                //       ),
                //     ],
                //   ),
                //   ))

                // ),
                child: Container(
                  height: 400,
                  child: SingleChildScrollView(
                      child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Card(
                        child: Padding(
                          padding: EdgeInsets.all(3),
                          
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("There is a girl which is hot ..."),
                          subtitle: Text(
                              "She is a beautiful girl, her name is sadia priti, and she is super ..."),
                        ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: EdgeInsets.all(3),
                          
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRV5esYp5KbWpXj0i9BGUPEr14H9qrpzlOh8w&usqp=CAU'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("There Covid 19 pandamic s ..."),
                          subtitle: Text(
                              "This codiv 19 pandamic is more dangerious situation..."),
                        ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: EdgeInsets.all(3),
                          
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQcbrDaD6uVRDckkcgFURHraUL1hrf4aoYiiA&usqp=CAU'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("Pakundia high school ..."),
                          subtitle: Text(
                              "One of the most biggest school in pakundia ..."),
                        ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: EdgeInsets.all(3),
                          
                        child: ListTile(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => NewsDetails()));
                          },
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRK895oRa20G9NT7e_R3_W5oz0Vd4YRpmrwfA&usqp=CAU'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("A woman killed by this teen . ..."),
                          subtitle: Text(
                              "In Pakundia there is a sukhiye union ..."),
                        ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: EdgeInsets.all(3),
                          
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://i1.rgstatic.net/ii/lab.file/AS%3A855572698116096%401580995835359_xl'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("There is a girl which is hot ..."),
                          subtitle: Text(
                              "She is a beautiful girl, her name is sadia priti, and she is super ..."),
                        ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: EdgeInsets.all(3),
                          
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSICg1sTaK_DqvezQsGYC4tJeSfPxvbErzqcQ&usqp=CAU'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("পাকুন্দিয়া আমার ভালবাসা ..."),
                          subtitle: Text(
                              "পাকুন্দিয়া আমার ভালবাসা  is sadia priti, and she is super ..."),
                        ),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("There is a girl which is hot ..."),
                          subtitle: Text(
                              "She is a beautiful girl, her name is sadia priti, and she is super ..."),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("There is a girl which is hot ..."),
                          subtitle: Text(
                              "She is a beautiful girl, her name is sadia priti, and she is super ..."),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("There is a girl which is hot ..."),
                          subtitle: Text(
                              "She is a beautiful girl, her name is sadia priti, and she is super ..."),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("There is a girl which is hot ..."),
                          subtitle: Text(
                              "She is a beautiful girl, her name is sadia priti, and she is super ..."),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("There is a girl which is hot ..."),
                          subtitle: Text(
                              "She is a beautiful girl, her name is sadia priti, and she is super ..."),
                        ),
                      ),
                      Card(
                        child: ListTile(
                          onTap: () {},
                          leading: CircleAvatar(
                            radius: 30.0,
                            backgroundImage: NetworkImage(
                                'https://media.newstracklive.com/uploads/entertainment-news/bollywood-news/Jan/13/big_thumb/uiyl_5e1c0e524a4a4.jpg'),
                            backgroundColor: Colors.transparent,
                          ),
                          title: Text("There is a girl which is hot ..."),
                          subtitle: Text(
                              "She is a beautiful girl, her name is sadia priti, and she is super ..."),
                        ),
                      ),
                    ],
                  )),
                ))
          ],
        ));
  }
}

Widget _myListView(BuildContext context) {
  return Card(
    child: ListTile(
      leading: Icon(Icons.event),
      title: Text("somthing went wrong"),
    ),
  );

  // final titles = ['bike', 'boat', 'bus', 'car',
  // 'railway', 'run', 'subway', 'transit', 'walk'];

  // final icons = [Icons.directions_bike, Icons.directions_boat,
  // Icons.directions_bus, Icons.directions_car, Icons.directions_railway,
  // Icons.directions_run, Icons.directions_subway, Icons.directions_transit,
  // Icons.directions_walk];

  // return ListView.builder(
  //   itemCount: titles.length,
  //   itemBuilder: (context, index) {
  //     return Card(
  //       child: ListTile(
  //         leading: Icon(icons[index]),
  //         title: Text(titles[index]),
  //       ),
  //     );
  //   },
  // );
}

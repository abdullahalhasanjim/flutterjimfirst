// import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';

class DemoLocalization {
  DemoLocalization(this.locale);

  final Locale locale;

  static DemoLocalization of(BuildContext context) {
    return Localizations.of<DemoLocalization>(context, DemoLocalization);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'title': 'Hello World',
    },
    'bn': {
      'title': 'Hola Mundo',
    },
  };

  // String get title {
  //   return _localizedValues[locale.languageCode]['title'];
  // }

  // Map<String, String> _localizedValue;

  // Future load() async {
  //    String jsonStrings = await rootBundle.loadString('lib/lang/${locale.languageCode}.json');
  //    Map<String, dynamic> mappedJson = json.decode(jsonStrings);
  //    _localizedValue = mappedJson.map((key, value) => MapEntry(key, value.toString()));
  // }
  String getTranslateValue(String key) {
    // return _localizedValue[key];
    return _localizedValues[locale.languageCode]['title'];
  }

  static const LocalizationsDelegate<DemoLocalization> delegate = DemoLocalizationDelegate();
}

// class _DemoLocalizationDelegate extends LocalizationsDelegate<DemoLocalization> {
//   const _DemoLocalizationDelegate();
//   @override
//   bool isSupported(Locale locale) => ['en', 'bn'].contains(locale.languageCode);

//   @override
//   // Future<DemoLocalization> load(Locale locale) async{
//   //   DemoLocalization localizations = new DemoLocalization(locale);
//   //   await localizations.load();
//   //   return localizations;
//   // }
//    @override
//   Future<DemoLocalization> load(Locale locale) {
//     return SynchronousFuture<DemoLocalization>(DemoLocalization(locale));
//   }

//   @override
//   bool shouldReload(_DemoLocalizationDelegate old) => false;
// }

class DemoLocalizationDelegate extends LocalizationsDelegate<DemoLocalization> {
  const DemoLocalizationDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);

  @override
  Future<DemoLocalization> load(Locale locale) {
    return SynchronousFuture<DemoLocalization>(DemoLocalization(locale));
  }

  @override
  bool shouldReload(DemoLocalizationDelegate old) => false;
}
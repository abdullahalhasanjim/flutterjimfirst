import 'package:flutter/material.dart';
import 'package:jimfirst/pakundia/news/pk_news_details.dart';
import 'package:jimfirst/pakundia/pk_end_drawer.dart';

class PkNews extends StatelessWidget { 
  @override 
  Widget build (BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('প্রজ্ঞাপন লিস্ট'),backgroundColor: Colors.teal),
      endDrawer: PkEndDrawer(),
      body: TodaysPakundiaNews(),
    );
  }
}



class TodaysPakundiaNews extends StatefulWidget {
    @override
  _TodaysPakundiaNewsState createState() => new _TodaysPakundiaNewsState();
}

class _TodaysPakundiaNewsState extends State<TodaysPakundiaNews> {

  final List newsList = [
    {'id' : 1, 'title' : 'করোনাভাইরাসের বিস্তার রোধে এখনই ', 'imageUrl' : 'assets/Friends/news/news11.jpg','date' : '08-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    {'id' : 2, 'title' : 'পাকুন্দিয়া একটি আদর্শ উপজেলা ', 'imageUrl' : 'assets/Friends/news/news1.jpg','date' : '09-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    {'id' : 3, 'title' : 'আমরা পাকুন্দিয়া কে ভালোবাসি ', 'imageUrl' : 'assets/Friends/news/news2.jpg','date' : '08-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    {'id' : 4, 'title' : 'নারান্দি একটি ইউনিয়ন পাকুন্দিয়ার ', 'imageUrl' : 'assets/Friends/news/news3.jpg','date' : '18-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    {'id' : 5, 'title' : 'করোনাভাইরাসের বিস্তার রোধে এখনই ', 'imageUrl' : 'assets/Friends/news/news4.jpg','date' : '04-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    {'id' : 6, 'title' : 'চল কিছু একটা করি  ', 'imageUrl' : 'assets/Friends/news/news5.jpg','date' : '15-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    {'id' : 7, 'title' : 'এখনেই সময় কিছু একটা করার  ', 'imageUrl' : 'assets/Friends/news/news6.jpg','date' : '08-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    {'id' : 8, 'title' : 'এখনেই না করলে কখন  ', 'imageUrl' : 'assets/Friends/news/news7.jpg','date' : '08-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    {'id' : 9, 'title' : 'কি বলিস তুই কিছু একটা বল  ', 'imageUrl' : 'assets/Friends/news/news8.jpg','date' : '15-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    {'id' : 10, 'title' : 'সবাই সবার মত কেউ কার ও মত না ', 'imageUrl' : 'assets/Friends/news/news9.jpg','date' : '10-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    {'id' : 11, 'title' : 'রক্ত দিলে অনেক ভাল , হাত হাত রাখুন ', 'imageUrl' : 'assets/Friends/news/news10.jpg','date' : '08-10-2020', 'description' : 'আমার পাকুন্দিয়া একটা সামাজিক সেবা মুলক সফটওয়্যার । এইখানে থেকে আশা করি সবাই কিছু না কিছু সেবা পেয়ে থাকবে , শুধুই সেবা দিতে পারবে তা না সেবা দেওয়া ও নেওয়া সব ইয়েই করা যাবে এই এক এপ থেকে '},
    ];
  Widget build(BuildContext context) {
    return  Container(
      child: ListView.builder(
        itemCount: newsList.length,
        itemBuilder: (BuildContext context,int index) {
          return ListTile(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PkNewsDetails(newsTitle : newsList[index]['title'], newsImage : newsList[index]['imageUrl'],newsDescription : newsList[index]['description'] )));
            },
            // leading: CircleAvatar(
            //   radius: 25,
            //   backgroundImage: AssetImage(newsList[index]['imageUrl']), 
            // ),
            leading: Container(
              height: 70,
              width: 70,
              decoration: BoxDecoration(
                image : DecorationImage(image: AssetImage(newsList[index]['imageUrl']),fit: BoxFit.fill),
                borderRadius: BorderRadius.circular(8)
              ),
            ),
            title: Text(newsList[index]['title']),
            subtitle: Text(newsList[index]['date']),
          );
      }
      ),
    );
  }
}

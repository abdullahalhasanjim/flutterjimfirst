import 'package:flutter/material.dart';
import 'package:jimfirst/practice/matrimonial/matrimonial.dart';
import 'about_us.dart';
import 'about_pakundia.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../login.dart';
import 'blood_group.dart';
import '../product.dart';
import 'drawer_account.dart';

class EndDrawer extends StatelessWidget {
  @override 
  Widget build(BuildContext context) {
    return ClipPath(
          clipper:  _DraweJimClipper(),
          child: Drawer(
          elevation: 20.0,
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(70, 10, 0, 10),
                child: UserAccountsDrawerHeader(
                accountName: Text('Mr Jim'),
                accountEmail: Text('jim.com@gmail.com'),
                currentAccountPicture :  Container(
                        decoration:BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image:  NetworkImage(
                              "https://femina.wwmindia.com/content/2019/aug/sonamkapoor1565335875.jpg",
                            ),
                          ),
                        ),
                      ),
                // currentAccountPicture: 
                //     Image.network('https://femina.wwmindia.com/content/2019/aug/sonamkapoor1565335875.jpg'),
                decoration: BoxDecoration(color: Colors.blueAccent),
              ),),
              Container(
                height: 400,
                child : SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(55, 0, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        ListTile(
                          leading: Icon(Icons.home),
                          title: Text('Home'),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DrawerAccount()));
                          },
                        ),
                        ListTile(
                          leading: Icon(Icons.person),
                          title: Text('Profile'),
                        ),
                        ListTile(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ProductPage()));
                          },
                          leading: Icon(Icons.shopping_cart),
                          title: Text('Products'),
                        ),
                        ListTile(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => BloodGroup()));
                          },
                          leading: Icon(Icons.people),
                          title: Text('Bloods'),
                        ),
                        ListTile(
                          leading: Icon(Icons.local_hospital),
                          title: Text('Health'),
                        ),
                        ListTile(
                          leading: Icon(Icons.local_library),
                          title: Text('Education'),
                        ),
                        ListTile(
                          leading: Icon(Icons.pages),
                          title: Text('News'),
                        ),
                        ListTile(
                          leading: Icon(Icons.business),
                          title: Text('Business'),
                        ),
                        ListTile(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => Matrimonial()));
                          },
                          leading: Icon(Icons.contacts),
                          title: Text('Matrimonial'),
                        ),
                        ListTile(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => AboutUs()));
                          },
                          leading: Icon(Icons.info),
                          title: Text('About Us'),
                        ),
                        ListTile(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => AboutPakundia()));
                          },
                          leading: Icon(Icons.more),
                          title: Text('About Pakundia'),
                        ),
                      ],
                    ),
                  ))
              ),
              Container(
                  margin: EdgeInsets.only(left: 100),
                  // This container holds all the children that will be aligned
                  // on the bottom and should not scroll with the above ListView
                  child: SettingsSignout(),
              
            )
            
            ],
          ),
      ),
       );
  }
}


class SettingsSignout extends StatefulWidget {
    @override
  _SettingsSignoutState createState() => new _SettingsSignoutState();
}

class _SettingsSignoutState extends State<SettingsSignout> {
  SharedPreferences sharedPreferences;

  void initState() {
    super.initState();
  }

  signOut () async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("token", null);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
        (Route<dynamic> route) => false);
  }

  Widget build(BuildContext context) {
    return  Container(
                      child: Column(
                    children: <Widget>[
                      Divider(),
                      ListTile(
                          leading: Icon(Icons.settings),
                          title: Text('Settings')),
                      ListTile(
                          onTap: () {
                            signOut();
                          },
                          leading: Icon(Icons.exit_to_app),
                          title: Text('Sign Out'))
                    ],
                  )
                );
  }
}

class _DraweJimClipper extends CustomClipper<Path> {
  Path getClip(Size size){
      Path path = Path();
      path.moveTo(95, 0);
      path.quadraticBezierTo(30,size.height/2, 95, size.height);
      path.lineTo(size.width, size.height);
      path.lineTo(size.width, 0);

      return path;
    }
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}
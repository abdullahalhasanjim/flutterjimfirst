import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
// import 'package:jimfirst/localization/demo_localization.dart';
import 'package:jimfirst/routes/cumtom_routes.dart';
import 'package:jimfirst/routes/route_name.dart';
import 'login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dashboard.dart';
import 'pakundia/pakundia.dart';
import 'practice/practicehome.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter_localizations/flutter_localizations.dart';


// void main() => runApp(new MyApp());
void main() => runApp(EasyLocalization(
  child: MyApp(),
   supportedLocales: [
     Locale('en','EN'),
     Locale('bn','BN'),
     ],
    path: 'lib/lang')
  );

class MyApp extends StatefulWidget {

  // static void setLocale(BuildContext context,Locale locale) {
  //   _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
  //   state.setLocale(locale);
  // }
  static void setLocale(BuildContext context,String languageCode) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(languageCode);
  }
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
   void setLocale(languageCode) {
     switch(languageCode) {
      case 'en' : 
        setState(() {
          EasyLocalization.of(context).locale = Locale('en', 'EN');
        });
        break;
      case 'bn' : 
        setState(() {
          EasyLocalization.of(context).locale = Locale('bn', 'BN');
        });
        break;
      default : 
      setState(() {
          EasyLocalization.of(context).locale = Locale('en', 'EN');
        });
    }
   }
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      // routes: <String, WidgetBuilder>{
        // '/dashboard': (BuildContext context) => new DashboardPage(),
        // '/product': (BuildContext context) => new ProductPage(),
      // },
      home: new MyHomePage(),
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      // locale: _locale,
      // supportedLocales: [
      //   Locale('en','US'),
      //   Locale('bn','BD')
      // ],
      // localizationsDelegates: [
      //   // ... app-specific localization delegate[s] here
      //   DemoLocalization.delegate,
      //   GlobalMaterialLocalizations.delegate,
      //   GlobalWidgetsLocalizations.delegate,
      //   GlobalCupertinoLocalizations.delegate,
      // ],
      // localeResolutionCallback: (deviceLocale , supportedLocale) {
      //   for(var locale in supportedLocale) {
      //     if(locale.languageCode == deviceLocale.languageCode && locale.countryCode == deviceLocale.countryCode) {
      //       return deviceLocale;
      //     }
      //   }
      //   return supportedLocale.first;
      // },

      onGenerateRoute: CustomRouter.allRoute,
      initialRoute: homeRoute,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  SharedPreferences sharedPreferences;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  notificatoinShow() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        // _showItemDialog(message);
      },
      // onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // _navigateToItemDetail(message);
      },
    );
  }

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
    _firebaseMessaging.getToken().then((token) => 
       print("jim token : "+token)
    );
    notificatoinShow();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          (Route<dynamic> route) => false);
    }
  }

  logout() async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("token", null);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Stack(
      children: <Widget>[
        ClipPath(
          child: Container(color: Colors.blue[700].withOpacity(0.8)),
          clipper: GetClipper(),
        ),
        Positioned(
            width: 350.0,
            top: MediaQuery.of(context).size.height / 5,
            child: Column(
              children: <Widget>[
                Container(
                    width: 150.0,
                    height: 150.0,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        image: DecorationImage(
                            image: NetworkImage(
                                'https://ocpl.com.bd/wp-content/uploads/2019/05/Jim-300x300.jpeg'),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.all(Radius.circular(75.0)),
                        boxShadow: [
                          BoxShadow(blurRadius: 7.0, color: Colors.black)
                        ])),
                SizedBox(height: 90.0),
                Text(
                  'Mr Jim',
                  style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Montserrat'),
                ),
                SizedBox(height: 15.0),
                Text(
                  'Points : 45',
                  style: TextStyle(
                      fontSize: 17.0,
                      fontStyle: FontStyle.italic,
                      fontFamily: 'Montserrat'),
                ),
                SizedBox(height: 25.0),
                Container(
                    height: 30.0,
                    width: 95.0,
                    child: Material(
                      borderRadius: BorderRadius.circular(20.0),
                      shadowColor: Colors.greenAccent,
                      color: Colors.green,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {},
                        child: Center(
                          child: Text(
                            'Edit Profile',
                            style: TextStyle(
                                color: Colors.white, fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    )),
                SizedBox(height: 25.0),
                Container(
                    height: 30.0,
                    width: 95.0,
                    child: Material(
                      borderRadius: BorderRadius.circular(20.0),
                      shadowColor: Colors.blueAccent,
                      color: Colors.blue,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {
                          // Navigator.of(context).pushNamed('/dashboard'); 
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DashboardPage()));
                        },
                        child: Center(
                          child: Text(
                            'Dashboard',
                            style: TextStyle(
                                color: Colors.white, fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    )),
                SizedBox(height: 25.0),
                Container(
                    height: 30.0,
                    width: 95.0,
                    child: Material(
                      borderRadius: BorderRadius.circular(20.0),
                      shadowColor: Colors.blueAccent,
                      color: Colors.orange,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {
                          // Navigator.of(context).pushNamed('/product');
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => Pakundia()));
                        },
                        child: Center(
                          child: Text(
                            'Pakundia',
                            style: TextStyle(
                                color: Colors.white, fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    )),
                    SizedBox(height: 25.0),
                Container(
                    height: 30.0,
                    width: 95.0,
                    child: Material(
                      borderRadius: BorderRadius.circular(20.0),
                      shadowColor: Colors.blueAccent,
                      color: Colors.purple,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {
                          // Navigator.of(context).pushNamed('/product');
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PracticeHome()));
                        },
                        child: Center(
                          child: Text(
                            'Practice',
                            style: TextStyle(
                                color: Colors.white, fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    )),
                SizedBox(height: 25.0),
                Container(
                    height: 30.0,
                    width: 95.0,
                    child: Material(
                      borderRadius: BorderRadius.circular(20.0),
                      shadowColor: Colors.redAccent,
                      color: Colors.red,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: () {
                          logout();
                        },
                        child: Center(
                          child: Text(
                            'Log out',
                            style: TextStyle(
                                color: Colors.white, fontFamily: 'Montserrat'),
                          ),
                        ),
                      ),
                    ))
              ],
            ))
      ],
    ));
  }
}

class GetClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height / 1.9);
    path.lineTo(size.width + 125, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}

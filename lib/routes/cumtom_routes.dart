import 'package:flutter/material.dart';
import 'package:jimfirst/main.dart';
import 'package:jimfirst/pages/not_found.dart';
import 'package:jimfirst/pages/test.dart';
import 'package:jimfirst/routes/route_name.dart';

class CustomRouter {
  static Route<dynamic> allRoute(RouteSettings settings) {
    switch(settings.name) {
      case homeRoute:
       return MaterialPageRoute(builder: (_) => MyHomePage());
       break;
      case testRoute:
       return MaterialPageRoute(builder: (_) => Test());
       break;
       default:
       return MaterialPageRoute(builder: (_) => NotFound());
    }
  }
}
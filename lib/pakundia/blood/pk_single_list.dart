import 'package:flutter/material.dart';
import 'package:jimfirst/pakundia/pk_end_drawer.dart';
import 'package:url_launcher/url_launcher.dart';

class PkSingleList extends StatelessWidget {
  final int groupIndex;
  PkSingleList({Key key, this.groupIndex}) : super(key: key);

  final List bloodUsers = [
    {
      'name': "jim",
      'image': 'assets/Friends/jim.jpg',
      'zone': 'Dhaka',
      'bloodGroup': 'A +',
      'mobile': '01736608048'
    },
    {
      'name': "sagor",
      'image': 'assets/Friends/sagor.jpg',
      'zone': 'Japan',
      'bloodGroup': 'B +',
      'mobile': '01717022506'
    },
    {
      'name': "Tanvir",
      'image': 'assets/Friends/tanvir.jpg',
      'zone': 'Pakundia',
      'bloodGroup': 'O +',
      'mobile': '01937977207'
    },
    {
      'name': "kawser",
      'image': 'assets/Friends/kawser.jpg',
      'zone': 'Dhaka',
      'bloodGroup': 'A +',
      'mobile': '01736608048'
    },
    {
      'name': "shovon",
      'image': 'assets/Friends/shovon.jpg',
      'zone': 'Mymnesing',
      'bloodGroup': 'A +',
      'mobile': '01736608048'
    },
    {
      'name': "robin",
      'image': 'assets/Friends/robin.jpg',
      'zone': 'Kishoreganj',
      'bloodGroup': 'A +',
      'mobile': '01736608048'
    },
    {
      'name': "shovo",
      'image': 'assets/Friends/shovo.jpg',
      'zone': 'Dhaka',
      'bloodGroup': 'A +',
      'mobile': '01736608048'
    },
    {
      'name': "samrat",
      'image': 'assets/Friends/samrat.jpg',
      'zone': 'Kishoreganj',
      'bloodGroup': 'A +',
      'mobile': '01736608048'
    },
    {
      'name': "priti",
      'image': 'assets/Friends/sadia.jpg',
      'zone': 'Kishoreganj',
      'bloodGroup': 'A +',
      'mobile': '01736608048'
    },
    {
      'name': "tasnim",
      'image': 'assets/Friends/priti2.jpg',
      'zone': 'Pakundia',
      'bloodGroup': 'B +',
      'mobile': '01717022506'
    },
    {
      'name': "saifulvai",
      'image': 'assets/Friends/saifulvai.jpg',
      'zone': 'Pakundia',
      'bloodGroup': 'B +',
      'mobile': '01717022506'
    },
    {
      'name': "sumonvai",
      'image': 'assets/Friends/sumonvai.jpg',
      'zone': 'Dhaka',
      'bloodGroup': 'B +',
      'mobile': '01717022506'
    },
    {
      'name': "nayim",
      'image': 'assets/Friends/nayim.jpg',
      'zone': 'Pakundia',
      'bloodGroup': 'B +',
      'mobile': '01717022506'
    },
    {
      'name': "nasir",
      'image': 'assets/Friends/nasir.jpg',
      'zone': 'Pakundia',
      'bloodGroup': 'B +',
      'mobile': '01717022506'
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ডোনার লিস্ট'),
        backgroundColor: Colors.pink,
      ),
      endDrawer: PkEndDrawer(),
      body: Container(
        child: ListView.builder(
            itemCount: bloodUsers.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: CircleAvatar(
                    radius: 30.0,
                    backgroundImage: AssetImage(bloodUsers[index]['image'])),
                title: Text(capitalize(bloodUsers[index]['name']) +
                    ", " +
                    bloodUsers[index]['bloodGroup']),
                subtitle: Text(bloodUsers[index]['mobile'] +
                    ", " +
                    bloodUsers[index]['zone']),
                trailing: ListPopupMenu(bloodUsers[index]['mobile']),
              );
            }),
      ),
    );
  }
}

String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

dynamic ListPopupMenu(mobile) {
  return PopupMenuButton(
      offset: Offset(0, 35),
      onSelected: (value) async {
        switch (value) {
          case 'call':
            if (await canLaunch("tel:$mobile")) {
              await launch("tel:$mobile");
            }
            break;
          case 'sms':
            if (await canLaunch("sms:$mobile")) {
              await launch("sms:$mobile");
            }
            break;
          case 'view':
            print('this is view ');
            const url = 'https://www.bbc.com/bengali/news-44485273';
            if (await canLaunch(url)) {
              await launch(url);
            } else {
              throw 'Could not launch $url';
            }
            break;
        }
      },
      itemBuilder: (BuildContext context) {
        return [
          PopupMenuItem(
              value: 'call',
              child: Row(children: <Widget>[
                Icon(Icons.phone),
                SizedBox(width: 10),
                Text("Call")
              ])),
          PopupMenuItem(
              value: 'sms',
              child: Row(children: <Widget>[
                Icon(Icons.sms),
                SizedBox(width: 10),
                Text("SMS")
              ])),
          PopupMenuItem(
              value: 'view',
              child: Row(children: <Widget>[
                Icon(Icons.remove_red_eye),
                SizedBox(width: 10),
                Text("View")
              ]))
        ];
      });
}

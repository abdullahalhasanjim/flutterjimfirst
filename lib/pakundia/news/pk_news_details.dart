import 'package:flutter/material.dart';
import 'package:jimfirst/pakundia/pk_end_drawer.dart';

class PkNewsDetails extends StatelessWidget { 
  final String newsTitle,newsImage,newsDescription;
  PkNewsDetails({Key key,this.newsTitle,this.newsImage,this.newsDescription}) : super(key:key);
  @override 
  Widget build (BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('প্রজ্ঞাপন'),backgroundColor: Colors.teal,),
      endDrawer: PkEndDrawer(),
      body: Column(
        children: <Widget>[
         Flexible(
           flex: 3,
           child: Container(
            padding: EdgeInsets.all(3),
            width: MediaQuery.of(context).size.width-5,
            height: 200,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(newsImage),
              )
            ),
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
              margin: EdgeInsets.only(top:5),
              alignment: Alignment.topCenter,
              child: Text(newsTitle,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold ),),
            )
          ),
          Flexible(
              flex: 8,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: ListView(
                  children: <Widget> [
                    Text(newsDescription,textAlign: TextAlign.justify,),
                    Text(newsDescription,textAlign: TextAlign.justify),
                    Text(newsDescription,textAlign: TextAlign.justify),
                    Text(newsDescription,textAlign: TextAlign.justify),
                    Text(newsDescription,textAlign: TextAlign.justify),
                    Text(newsDescription,textAlign: TextAlign.justify),
                    Text(newsDescription,textAlign: TextAlign.justify),
                    Text(newsDescription,textAlign: TextAlign.justify),
                    Text(newsDescription,textAlign: TextAlign.justify),
                    Text(newsDescription,textAlign: TextAlign.justify),
                    Text(newsDescription,textAlign: TextAlign.justify),
                  ]
                ),
              )
            ), 
        ],
      ),
    );
  }
}
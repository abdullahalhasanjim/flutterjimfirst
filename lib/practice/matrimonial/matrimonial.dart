import 'package:flutter/material.dart';
import '../end_drawer.dart';
import 'package:flip_card/flip_card.dart';

class Matrimonial extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(" পাত্র / পাত্রী   চাই ")),
      endDrawer: EndDrawer(),
      body: MatrimonialList(),
    );
  }
}

class MatrimonialList extends StatelessWidget {
  final List<dynamic> teams = [
    {'name' : "Abdullah Al Hasan", 'designation' : "Software Engineer", 'jobDescription' : "Business Automation", 'height' : '5 feet 7 inchee', 'weight' : "72 kg", 'image' : 'assets/Friends/jim.jpg'},
    {'name' : "Rubayeth Kabir Sagor", 'designation' : "Softare Engineer", 'jobDescription' : "Engineering at Japan", 'height' : '5 feet 2 inchee', 'weight' : "82 kg", 'image' : 'assets/Friends/sagor.jpg'},
    {'name' : "Tanvir Rahman",'designation' : "Doctor",'jobDescription' : "MBBS,FCPS, Mithamoin",'height' : '5 feet 5 inchee','weight' : "72 kg", 'image' : 'assets/Friends/tanvir.jpg'},
    {'name' : "Kawser Ahmed",'designation' : "EEE Engineer",'jobDescription' : "Business Automation", 'height' : '5 feet 10 inchee', 'weight' : "82 kg",'image' : 'assets/Friends/kawser.jpg'},
    {'name' : "Shovon Mahmud", 'designation' : "Textile Engineer",'jobDescription' : "Business Automation",'height' : '5 feet 10 inchee', 'weight' : "72 kg", 'image' : 'assets/Friends/shovon.jpg'},
    {'name' : "Robin Rahman", 'designation' : "Custom Excutive",'jobDescription' : "Business Automation",'height' : '5 feet 10 inchee', 'weight' : "72 kg", 'image' : 'assets/Friends/robin.jpg'},
    {'name' : "Jahid Hasan Shovo",'designation' : "Doctor",'jobDescription' : "MBBS,FCPS, Sirajganj", 'height' : '5 feet 7 inchee', 'weight' : "92 kg",'image' : 'assets/Friends/shovo.jpg'},
    {'name' : "Samrat Mahmud", 'designation' : "Marine Engineer",'jobDescription' : "Marine at Narayanganj",'height' : '5 feet 6 inchee', 'weight' : "72 kg", 'image' : 'assets/Friends/samrat.jpg'},
    {'name' : "Sadia Priti",'designation' : "Teacher ",'jobDescription' : "Kolatoli University",'height' : '5 feet 4 inchee', 'weight' : "62 kg", 'image' : 'assets/Friends/pritsadia.jpg'},
    {'name' : "Tasnim Priti", 'designation' : "Teacher ",'jobDescription' : "Kolatoli University",'height' : '5 feet 3 inchee', 'weight' : "52 kg",'image' : 'assets/Friends/tasnimpriti.jpg'},
    {'name' : "Saiful Islam", 'designation' : "Head of Marketing",'jobDescription' : "Wedevs Solutions",'height' : '5 feet 7 inchee', 'weight' : "72 kg", 'image' : 'assets/Friends/saifulvai.jpg'},
    {'name' : "Saman sarkar",'designation' : "Senior Excutive",'jobDescription' : "Acme Farmaceticals",'height' : '5 feet 7 inchee', 'weight' : "72 kg",  'image' : 'assets/Friends/sumonvai.jpg'},
    {'name' : "Nayim Mota", 'designation' : "Businessman",'jobDescription' : "Restuarent at Dhaka", 'height' : '5 feet 11 inchee', 'weight' : "102 kg", 'image' : 'assets/Friends/nayim.jpg'},
    {'name' : "Al Jubayer",'designation' : "Teacher",'jobDescription' : "Kolatoli University", 'height' : '5 feet 7 inchee', 'weight' : "72 kg", 'image' : 'assets/Friends/nasir.jpg'},
    ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(
        crossAxisCount: 2,
        children: List.generate(teams.length, (index) {
          return Center(
            child: GestureDetector(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: FlipCard(
                  direction: FlipDirection.VERTICAL,
                  front: FrontCard(imageUrl: teams[index]['image']),
                  back: BackCard(
                      name: teams[index]['name'], designation : teams[index]['designation'],jobDescription: teams[index]['jobDescription'],height: teams[index]['height'], weight: teams[index]['weight'], color: Colors.pink),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}

class FrontCard extends StatelessWidget {
  final String imageUrl;
  const FrontCard({Key key, this.imageUrl}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
          elevation: 10,
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: Image.asset(imageUrl,
            fit: BoxFit.fill,
            height: 250,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        );
  }
}

class BackCard extends StatelessWidget {
  final String name, designation,jobDescription,height,weight;
  final MaterialColor color;
  const BackCard({Key key, this.name, this.designation,this.jobDescription, this.height, this.weight,this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      elevation: 10,
      // child: Center(
      //   child: Text(
      //     name,
      //     style: TextStyle(
      //         color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
      //   ),
      // ),
      child: Center(
        child: Column(
        children : <Widget> [
          Container(
            child: Padding(
              padding: EdgeInsets.all(2),
              child: Text(name,style: TextStyle(color: Colors.white),),
            ),
          ),
          Container(
            child: Padding(
              padding: EdgeInsets.all(2),
              child: Text(designation,style: TextStyle(color: Colors.white),),
            ),
          ),
          Container(
            child: Padding(
              padding: EdgeInsets.all(2),
              child: Text(jobDescription,style: TextStyle(color: Colors.white),),
            ),
          ),
          Container(
            child: Padding(
              padding: EdgeInsets.all(2),
              child: Text(height,style: TextStyle(color: Colors.white),),
            ),
          ),
          Container(
            child: Padding(
              padding: EdgeInsets.all(2),
              child: Text(weight,style: TextStyle(color: Colors.white),),
            ),
          ),
          Container(
            child : GestureDetector(
               child: Padding(
              padding: EdgeInsets.all(2),
              child: RaisedButton(onPressed: () {

                  },
              child: Text('More Info'),
              ),
            ),
            )
          ),
        ]
      ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'product.dart';

class MoreDetailsPage extends StatefulWidget {
  final heroTag;
  final foodName;
  final foodPrice;

  MoreDetailsPage({this.heroTag, this.foodName, this.foodPrice});

  @override
  _MoreDetailsPageState createState() => _MoreDetailsPageState();
}

class _MoreDetailsPageState extends State<MoreDetailsPage> {
  var selectedCard = 'WEIGHT';

  @override
  Widget build(BuildContext context) {
        return new Scaffold(
      appBar: AppBar(
        title: Text('নাচের খাঁটি সরিষার তেল '),
      ),
      body:  Column(
        children: <Widget>[
          Container(
            height: 250,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Image.network('https://www.citygroup.com.bd/public/backEnd/img/product/1591936941sun-vanaspati.png'),
            ),
          ),
          Text(
            'Price: ৳ 5000',
            style: TextStyle(
              fontSize: 30,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              'This is the peoduct description onek kichu eikhaen lahgoh tho og agoghoshgo hogoglsl goghoghohoss',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          SizedBox(height: 20.0),
          Text(
            'Delivery Description',
            style: TextStyle(
              fontSize: 30,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              'This is the delivery description onek kichu eikhaen lahgoh tho og agoghoshgo',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
          SizedBox(height: 20.0),
          InkWell(
                        onTap :() {
                          Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProductPage()
                        ));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0), bottomLeft: Radius.circular(25.0), bottomRight: Radius.circular(25.0)),
                            color: Colors.pink
                          ),
                          height: 50.0,
                          child: Center(
                            child: Text(
                              'Confirm Order',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Montserrat'
                              )
                            ),
                          ),
                        ),
                      )
        ],
      ),
      
    );
  }
  }


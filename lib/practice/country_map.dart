import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jimfirst/practice/end_drawer.dart';

class CountryMap extends StatelessWidget {

  final String countryName;
  final List latLang;
  CountryMap({Key key,this.countryName,this.latLang}) : super(key:key);

  Completer<GoogleMapController> _controller = Completer();

  // static final CameraPosition _kGooglePlex = CameraPosition(
  //   target: LatLng(37.42796133580664, -122.085749655962),
  //   zoom: 14.4746,
  // );

  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(countryName),backgroundColor: Colors.pink,),
      endDrawer: EndDrawer(),
      body : GoogleMap(initialCameraPosition: CameraPosition(
        // target: LatLng(24.312059, 90.708618),
        target: LatLng(latLang[0], latLang[1]),
        zoom: 6,
      ),
      onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },)
    );
  }
}

class PakundiaMap extends StatelessWidget {

  Completer<GoogleMapController> _controller = Completer();
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Pakundia"),backgroundColor: Colors.pink,),
      endDrawer: EndDrawer(),
      body : GoogleMap(initialCameraPosition: CameraPosition(
        target: LatLng(24.312059, 90.708618),
        zoom: 12,
      ),
      onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },)
    );
  }
}
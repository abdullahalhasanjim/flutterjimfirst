import 'package:flutter/material.dart';
import 'drayerLayout.dart';

class PracticeHome extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title : "User Profile",
      home : DrayerLayout()
    );
  }
}
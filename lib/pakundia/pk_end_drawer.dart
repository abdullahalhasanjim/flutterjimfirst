import 'package:flutter/material.dart';
import 'package:jimfirst/pakundia/about/pk_about_us.dart';
import 'package:jimfirst/pakundia/news/pk_news.dart';
import 'package:jimfirst/pakundia/products/pk_products.dart';
import 'pakundia.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../login.dart';
import 'blood/pk_blood.dart';

class PkEndDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
        clipper: _PkClipPath(),
        child: Drawer(
          elevation: 20,
          child: Column(children: <Widget>[
            Flexible(
                flex: 2,
                child: Container(
                  margin: EdgeInsets.only(left: 80),
                  child: UserAccountsDrawerHeader(
                    accountName: Text("Mr jim"),
                    accountEmail: Text('01736608048'),
                    currentAccountPicture: Container(
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage('assets/Friends/sonam.jpg'))),
                    ),
                    decoration: BoxDecoration(color: Colors.blueAccent),
                  ),
                )),
            Flexible(
                flex: 4,
                child: Container(
                    margin: EdgeInsets.only(left: 75),
                    child: ListView(children: <Widget>[
                      ListTile(
                        leading: Icon(Icons.home),
                        title: Text('Home'),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => Pakundia()));
                        },
                      ),
                      ListTile(
                        leading: Icon(Icons.person),
                        title: Text('Profile'),
                      ),
                      ListTile(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PkProducts()));
                        },
                        leading: Icon(Icons.shopping_cart),
                        title: Text('Products'),
                      ),
                      ListTile(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PkBlood()));
                        },
                        leading: Icon(Icons.people),
                        title: Text('Bloods'),
                      ),
                      ListTile(
                        leading: Icon(Icons.local_hospital),
                        title: Text('Health'),
                      ),
                      ListTile(
                        leading: Icon(Icons.local_library),
                        title: Text('Education'),
                      ),
                      ListTile(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => PkNews()));
                        },
                        leading: Icon(Icons.pages),
                        title: Text('News'),
                      ),
                      ListTile(
                        leading: Icon(Icons.business),
                        title: Text('Business'),
                      ),
                      ListTile(
                        onTap: () {
                          // Navigator.of(context).push(MaterialPageRoute(
                          //   builder: (context) => Matrimonial()));
                        },
                        leading: Icon(Icons.contacts),
                        title: Text('Matrimonial'),
                      ),
                      ListTile(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PkAboutUs()));
                        },
                        leading: Icon(Icons.info),
                        title: Text('About Us'),
                      ),
                      ListTile(
                        onTap: () {
                          // Navigator.of(context).push(MaterialPageRoute(
                          //   builder: (context) => AboutPakundia()));
                        },
                        leading: Icon(Icons.more),
                        title: Text('About Pakundia'),
                      ),
                    ]))),
            Flexible(
              flex: 1,
              child: SettingsSignout(),
            )
          ]),
        ));
  }
}

class _PkClipPath extends CustomClipper<Path> {
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(100, 0);
    /*
    path.lineTo(50, size.height/2);
    path.lineTo(100, size.height);
    */
    path.quadraticBezierTo(50, size.height / 2, 100, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

class SettingsSignout extends StatefulWidget {
  @override
  _SettingsSignoutState createState() => new _SettingsSignoutState();
}

class _SettingsSignoutState extends State<SettingsSignout> {
  SharedPreferences sharedPreferences;

  void initState() {
    super.initState();
  }

  signOut() async {
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("token", null);
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
        (Route<dynamic> route) => false);
  }

  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left:80),
        child: ListView(
      children: <Widget>[
        ListTile(leading: Icon(Icons.settings), title: Text('Settings')),
        ListTile(
            onTap: () {
              signOut();
            },
            leading: Icon(Icons.exit_to_app),
            title: Text('Sign Out'))
      ],
    ));
  }
}

import 'package:flutter/material.dart';
import 'package:jimfirst/classes/language.dart';
// import 'package:jimfirst/localization/demo_localization.dart';
import 'package:jimfirst/routes/route_name.dart';
import '../main.dart';
import 'drayer_item.dart';
import 'drawer_account.dart';
import 'dummy/p1.dart';
import 'package:easy_localization/easy_localization.dart';

class DrayerLayout extends StatefulWidget {
  @override
  _DrayerLayoutState createState() => _DrayerLayoutState();
}

class _DrayerLayoutState extends State<DrayerLayout> {

  void _changedLanguage(Language language) {
    // print(language.name);
    // Locale _temp;
    // switch(language.languageCode) {
    //   case 'en' : 
    //     _temp = Locale(language.languageCode,'US');
    //     break;
    //   case 'bn' : 
    //     _temp = Locale(language.languageCode,'BD');
    //     break;
    //   default : 
    //   _temp = Locale(language.languageCode,'US');
    // }
    // print(_temp);
    // MyApp.setLocale(context,_temp);
    MyApp.setLocale(context,language.languageCode);
  }

  @override 
  Widget build(BuildContext context) {
    return new Scaffold( 
      appBar: AppBar(
        // title:(Text(DemoLocalization.of(context).getTranslateValue('title'))),
         title:(Text('name'.tr().toString())),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.0),
            child: DropdownButton(
              underline: SizedBox(),
              items: Language.languageList().map<DropdownMenuItem<Language>>((lang) => 
              DropdownMenuItem(
                value: lang,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(lang.flag),
                  Text(lang.name)
                ],
               ),
              )
              ).toList(), 
              onChanged: (Language language){
                _changedLanguage(language);
              },
              icon: Icon(Icons.language,color: Colors.white),
            ),
          )
        ],
        leading: Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.0),
            child: DropdownButton(
              underline: SizedBox(),
              items: Language.languageList().map<DropdownMenuItem<Language>>((lang) => 
              DropdownMenuItem(
                value: lang,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(lang.flag),
                  Text(lang.name)
                ],
               ),
              )
              ).toList(), 
              onChanged: (Language language){
                _changedLanguage(language);
              },
              icon: Icon(Icons.language,color: Colors.white),
            ),
          ),
          
      ),
      drawer: ClipPath(
        clipper: _DraweClipper(), 
        child: Drawer(
          child: SingleChildScrollView(
           child: Container(
            padding: const EdgeInsets.only(top: 50),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                DrawerItem (
                  onPressed : () {
                    Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DrawerAccount()));
                   },
                  text : '01 : Drawer-Account'
                ),
                DrawerItem (
                  onPressed : () { 
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => Dummy()));
                  },
                  text : 'My Practice'
                ),
                DrawerItem (
                  onPressed : () {
                    Navigator.pushNamed(context, testRoute);
                   },
                  text : 'Service'
                ),
                
                DrawerSubItem (
                  onPressed : () {},
                  text : "Location"
                ),
                DrawerSubItem (
                  onPressed : () { },
                  text : 'Narandi'
                ),
                DrawerSubItem (
                  onPressed : () { },
                  text : 'Hossendi'
                ),
                DrawerSubItem (
                  onPressed : () { },
                  text : 'PAtuyaannga'
                ),
                DrawerSubItem (
                  onPressed : () { },
                  text : 'Burudiya'
                ),
                DrawerItem (
                  onPressed : () { },
                  text : 'Arlo'
                ),
                DrawerItem (
                  onPressed : () { },
                  text : 'Flutter'
                ),
              ],
            )
          ),
        ),
      
          ),
        ),
    );
  }
}

class _DraweClipper extends CustomClipper<Path>  {
    Path getClip(Size size){
      Path path = Path();
      path.moveTo(95, 0);
      path.quadraticBezierTo(30,size.height/2, 95, size.height);
      path.lineTo(size.width, size.height);
      path.lineTo(size.width, 0);

      return path;
    }

    bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}
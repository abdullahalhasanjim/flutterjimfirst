import 'package:flutter/material.dart';
import 'package:jimfirst/pakundia/pk_end_drawer.dart';
import 'package:flip_card/flip_card.dart';

class PkAboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(" আমাদের সম্পর্কে ")),
      endDrawer: PkEndDrawer(),
      body: AboutUsListTeam(),
    );
  }
}

class AboutUsListTeam extends StatelessWidget {
  final List<dynamic> teams = [
    {'name' : "jim", 'image' : 'assets/Friends/jim.jpg'},
    {'name' : "saifulvai", 'image' : 'assets/Friends/saifulvai3.jpg'},
    {'name' : "sagor", 'image' : 'assets/Friends/sagor.jpg'},
    {'name' : "Tanvir", 'image' : 'assets/Friends/tanvir.jpg'},
    {'name' : "kawser", 'image' : 'assets/Friends/kawser.jpg'},
    {'name' : "shovon", 'image' : 'assets/Friends/shovon2.jpg'},
    {'name' : "shovon", 'image' : 'assets/Friends/shovon3.jpg'},
    {'name' : "shovon", 'image' : 'assets/Friends/shovon.jpg'},
    {'name' : "robin", 'image' : 'assets/Friends/robin.jpg'},
    {'name' : "shovo", 'image' : 'assets/Friends/shovo.jpg'},
    {'name' : "samrat", 'image' : 'assets/Friends/samrat.jpg'},
    {'name' : "priti", 'image' : 'assets/Friends/sadia.jpg'},
    {'name' : "priti2", 'image' : 'assets/Friends/tasnimpriti.jpg'},
    {'name' : "saifulvai", 'image' : 'assets/Friends/saifulvai.jpg'},
    {'name' : "sumonvai", 'image' : 'assets/Friends/sumonvai.jpg'},
    {'name' : "nayim", 'image' : 'assets/Friends/nayim.jpg'},
    {'name' : "nasir", 'image' : 'assets/Friends/nasir.jpg'},
    {'name' : "saifulvai", 'image' : 'assets/Friends/saifulvai2.jpg'},

    ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(
        crossAxisCount: 2,
        children: List.generate(teams.length, (index) {
          return Center(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: FlipCard(
                  direction: FlipDirection.HORIZONTAL,
                  front: FrontCard(imageUrl: teams[index]['image']),
                  back: BackCard(
                      name: teams[index]['name'], color: Colors.deepOrange),
                ),
              ),
          );
        }),
      ),
    );
  }
}

class FrontCard extends StatelessWidget {
  final String imageUrl;
  const FrontCard({Key key, this.imageUrl}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
          elevation: 10,
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: Image.asset(imageUrl,
            fit: BoxFit.fill,
            height: 250,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        );
  }
}

class BackCard extends StatelessWidget {
  final String name;
  final MaterialColor color;
  const BackCard({Key key, this.name, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      elevation: 10,
      child: Center(
        child: Text(
          name,
          style: TextStyle(
              color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}

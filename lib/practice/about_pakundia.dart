import 'package:flutter/material.dart';
import 'country.dart';
import 'package:dio/dio.dart';


class AboutPakundia extends StatefulWidget {
  _AboutPakundiaState createState() => new _AboutPakundiaState();
}

class _AboutPakundiaState extends State<AboutPakundia> {
  bool isSearching = false;
  List countries = [];
  List filtredCountries = [];
  getAllCountries() async {
    var dio = Dio();
    Response response = await dio.get('https://restcountries.eu/rest/v2/all');
    return response.data;
  }

  void initState(){
    super.initState();
    getAllCountries().then( (data) {
      setState(() {
        countries = filtredCountries = data;
      });
    });
  }

  _filtredCountries(value) {
    setState(() {
      filtredCountries = 
    countries.where((country) => 
    country['name'].toLowerCase().contains(value.toLowerCase())).toList(); 
    });
    
  }

  Widget build(BuildContext context) {
  
    return Scaffold(
      appBar: AppBar(
        title: !isSearching ? Text("All Country") : TextField(
          onChanged: (value) {
            _filtredCountries(value);
          },
          style: TextStyle(color: Colors.white),
          decoration : InputDecoration(
            hintText: "Search your Country",
            icon: Icon(Icons.search, color: Colors.white,),
            focusColor: Colors.white,
            hintStyle: TextStyle(color:Colors.white)
            ),
          ),
        backgroundColor: Colors.pink,
        actions : <Widget>[
          isSearching ? 
          IconButton(
            icon: Icon(Icons.cancel),
            onPressed: () {
              setState(() {
                this.isSearching = false;
                filtredCountries = countries;
              });
            },
          ) :
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              setState(() {
                this.isSearching = true;
              });
            },
          )
        ],
        ),
        body: Container(
        child: Padding(
          padding: EdgeInsets.all(2),
          child: filtredCountries.length > 0  ? ListView.builder(
            itemCount: filtredCountries.length,
            itemBuilder: (BuildContext context,int index) {
            return GestureDetector(
              onTap : () {
                Navigator.of(context).push(MaterialPageRoute(builder : (context) => Country(filtredCountries[index])),);
              },
              child : Card (
                      elevation: 10,
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical : 10 , horizontal : 8),
                        child: Text(filtredCountries[index]['name']), 
                        ),
                      )
            );
           }
          ) : Center(child: CircularProgressIndicator(backgroundColor: Colors.pink,),)
        ),
      ),
    );
  }
}

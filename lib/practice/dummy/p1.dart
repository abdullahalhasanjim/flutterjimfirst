import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class Dummy extends StatelessWidget {
  @override 
  Widget build(BuildContext context) {

    final String _longText = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."; 

    return Scaffold(
      appBar: AppBar(
        title:Text("Title"),
        backgroundColor: Colors.pink,
        ),
        // endDrawer: EndDrawer(),
        // body: Container(
        //   child: Column(
        //     children: <Widget>[
        //       Image.asset('assets/Friends/tasnimpriti.jpg',height: 300,width: 300,alignment: Alignment.topCenter,),
        //     Row(
        //    children: <Widget>[
        //      Container(
        //        margin: EdgeInsets.all(10),
        //        padding : EdgeInsets.fromLTRB(8, 5, 8, 5),
        //        height: 50,
        //        width: 50,
        //        color: Colors.pink,
        //        foregroundDecoration: BoxDecoration(borderRadius: BorderRadius.circular(10),
        //         image: DecorationImage(image: AssetImage('assets/Friends/kawser.jpg'))
        //        ),
        //      ),
        //      Container(
        //        margin: EdgeInsets.all(10),
        //        padding : EdgeInsets.all(10),
        //        height : 50,
        //        width : 50,
        //        color: Colors.pink,
        //        foregroundDecoration: BoxDecoration(
        //          image : DecorationImage(image: AssetImage('assets/Friends/tanvir.jpg')),
        //          borderRadius: BorderRadius.circular(10)
        //        ),
        //      ),
        //      Container(
        //        margin: EdgeInsets.all(10),
        //        padding : EdgeInsets.all(10),
        //        height : 50,
        //        width : 50,
        //        color: Colors.pink,
        //        foregroundDecoration: BoxDecoration(
        //          image : DecorationImage(image: AssetImage('assets/Friends/sagor.jpg')),
        //          borderRadius: BorderRadius.circular(10)
        //        ),
        //      ),
        //      Container(
        //        margin: EdgeInsets.all(10),
        //        padding : EdgeInsets.all(10),
        //        height : 50,
        //        width : 50,
        //        color: Colors.pink,
        //        foregroundDecoration: BoxDecoration(
        //          image : DecorationImage(image: AssetImage('assets/Friends/jim.jpg')),
        //          borderRadius: BorderRadius.circular(10)
        //        ),
        //      ),
             
        //    ], 
        //   ),
            
        //     Container(
        //       height: 300,
        //       child : SingleChildScrollView(
        //         scrollDirection: Axis.vertical,
        //         child: Text(_longText + _longText + _longText + _longText),),
        //       )
            
        //     ],
        //   )
        // ),
        // body: ListView(
        //   children : <Widget> [
        //     Text(_longText+_longText + _longText + _longText)
        //   ]
        // ),
        // floatingActionButton: IconButton(
        //   icon: Icon(Icons.add), onPressed: () {
        //     print("this is button pressed");
        //   },
        //   ),

        // body : Column(
        //   children : <Widget> [
        //     Stack(children: <Widget>[
        //       Image.asset('assets/Friends/jim.jpg'),
        //       Positioned(
        //         bottom :40,right: 50, child: Text("Abdullah Al Hasan"),),
        //         Positioned( left: 20, bottom: 30,child: RaisedButton(child: Text("More"), onPressed: () {},))
        //     ],),
        //     SizedBox(height: 30),
        //     Row(children: <Widget>[
        //       _rowCell(Colors.deepOrange),
        //       _rowCell(Colors.pink),
        //       _rowCell(Colors.blue),
        //       _rowCell(Colors.red),
        //       _rowCell(Colors.green),
              
        //     ],)
        //   ]\
        
        // )
        // body : Container(
        //     child: ListView(
        //       children : <Widget> [
        //        ListTile(
        //          leading: CircleAvatar(
        //            child : Text("A")
        //          ),
        //          title : Text("This is the title"),
        //          subtitle: Text("jim@gmail.com"), 
        //          trailing: Icon(Icons.add_a_photo),
        //        )
        //       ]
        //     ),
        // ),
        // drawer: Drawer(
        //   elevation: 25,
        //   child: UserAccountsDrawerHeader(accountName: Text('MR jim'), accountEmail: Text('jim@gmail.com'))),
        // body: Container(
        //   child : ListView.builder(
        //     itemCount: 20,
        //     itemBuilder: (BuildContext context, int index) {
        //       return 
        //        ListTile(
        //          leading: CircleAvatar(
        //            child : Text(index.toString())
        //          ),
        //          title : Text("This is the title"),
        //          subtitle: Text("jim@gmail.com"), 
        //          trailing: Icon(Icons.add_a_photo),
        //        );
        //     },
        //   )
        // ),
        // drawer: ClipPath(
        //   clipper : _DraweJimClipper(),
        //   child : Drawer(
        //     elevation: 20,
        //     child: Text('it should be later'),
        //   ),
        // ),
        // body: Column(
        //   children: <Widget>[
        //     Flexible(
        //       flex: 2,
        //       child: Card(
        //         elevation: 10,
        //         child: Image.asset('assets/Friends/tasnimpriti.jpg'),),
        //       ),
        //       SizedBox(height: 10),
        //     Flexible( 
        //       flex : 1,
        //        child: Text('oshgophgw gosgh osgho ahgogho')
        //        ),
        //        SizedBox(height: 20),
        //     Flexible(
        //       flex: 6,
        //       child: ListView(
        //         children: <Widget>[
        //           Padding(padding: EdgeInsets.all(20),
        //           child:Text(_longText+_longText +_longText , textAlign: TextAlign.justify,),)
        //         ],
        //       )
        //     )
        //   ],
        // ),
        drawer: ClipPath(
          clipper: _GetDrawerClipPath (),
          child : Drawer(
           elevation: 10,
           child: Column(
            children: <Widget> [
              Flexible(
                flex: 2,
                child: Stack(children: <Widget>[
                // Image.asset('assets/Friends/tasnimpriti.jpg'),
                Container(
                  // height: 240,
                  decoration: BoxDecoration(
                    color: Colors.blue
                  ),
                ),
                Positioned(
                  left: 30,
                  top: 50,
                  child: Container(
                    height: 120,
                    width: 120,
                     child:  CircleAvatar(
                       backgroundImage: AssetImage('assets/Friends/jim.jpg')),
                  )),
                  Positioned(
                    left : 50,
                    top: 180,
                    child: Text("MR JIM", style : TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                  ),
                  Positioned(
                    left : 50,
                    top: 200,
                    child: Text("01680304447", style : TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                  ),
              ],),
              ),
              Flexible(
                flex: 3,
                child: ListView(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text("Home"),
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text("Home"),
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text("Home"),
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text("Home"),
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text("Home"),
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text("Home"),
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text("Home"),
                    ),
                    ListTile(
                      leading: Icon(Icons.home),
                      title: Text("Home"),
                    ),
                  ],
               ),
              ),
              Divider(),
              Flexible(
                flex: 1,
                child: ListView(
                  children: <Widget>[
                    ListTile(title: Text('Settings'), leading: Icon(Icons.settings),),
                    ListTile(title: Text('Sign Out'), leading: Icon(Icons.exit_to_app),),
                  ],
               ),
              )             
            ]
          ),
          ),
        ),
        body : FormDesignContainer(),
        floatingActionButton:  CircleAvatar(
          child : IconButton(icon: Icon(Icons.add_alert), onPressed: () {
          
        }),
        )
    );
  }

}

class _DraweJimClipper extends CustomClipper<Path> {
  Path getClip(Size size){
      Path path = Path();
      path.moveTo(95, 0);
      path.quadraticBezierTo(30,size.height/2, 95, size.height);
      path.lineTo(size.width, size.height);
      path.lineTo(size.width, 0);

      return path;
    }
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}


class _GetDrawerClipPath extends CustomClipper<Path>  {
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(200, 0);
    /*
    path.lineTo(size.height/2, size.width);
    path.lineTo(200, size.height);
    */
    path.quadraticBezierTo(250,size.width, 200, size.height);
    path.lineTo(0, size.height);
    path.lineTo(0, 0);

    return path;
  }

  bool shouldReclip(covariant CustomClipper<Path> oldCliper ) => true;
}


Widget _rowCellList (color) {
  return  Container(
              height: 50,
              width: 50,
              margin: EdgeInsets.all(3),
              decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(7)
              ),
              child: Icon(Icons.map),
            );
}

Widget _rowCell (color) {
  return Expanded(child:  Container(
              height: 50,
              width: 50,
              margin: EdgeInsets.all(3),
              decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(7)
              ),
              child: Icon(Icons.map),
            ));
}


class FormDesignContainer extends StatefulWidget {
    @override 
    _FormDesignContainerState createState() => _FormDesignContainerState();
}

class _FormDesignContainerState extends State<FormDesignContainer> {
  var formKey = GlobalKey<FormState>();
  File _pickedImage;

void handleSubmit() {
  if(formKey.currentState.validate()){
    formKey.currentState.save();
  }
}


Future _loadPicture(ImageSource source,context) async {
  File picked = await  ImagePicker.pickImage(source: source);
  if(picked != null) {
    _imageCrope(picked);
  }
  _closeDialog(context);
}


_imageCrope(File picked) async{
  File cropped = await ImageCropper.cropImage(
    androidUiSettings: AndroidUiSettings(
      statusBarColor: Colors.pink,
      toolbarColor: Colors.pink,
      toolbarTitle: "Crop Image",
      toolbarWidgetColor: Colors.white,
    ),
    sourcePath: picked.path,
  // aspectRatio : CropAspectRatio(ratioX: 1.0,ratioY: 1.0),
  maxWidth: 800,
  aspectRatioPresets: [
    CropAspectRatioPreset.original,
    CropAspectRatioPreset.ratio16x9,
    CropAspectRatioPreset.ratio4x3
  ]
  );
  if(cropped != null) {
    setState(() {
        _pickedImage = picked;
    });
  }
}

void _showPicturesOptionsDialog(BuildContext context){
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title : Text('Gallery'),
              trailing: Icon(Icons.camera),
              onTap : () {
                _loadPicture(ImageSource.gallery,context);
              },
            ),
            ListTile(
              title : Text('Camera'),
              trailing: Icon(Icons.add_a_photo),
              onTap : () {
                _loadPicture(ImageSource.camera,context);
              },
            ),
          ],
        ),
      )
    );
}

handleReset() {
 print('reset is pressed');
}

  @override 
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(
                    labelText: "Enter your Name",
                  ),
                  validator : (value) {
                      if(value.length == 0){
                         return 'Name is Required';
                      }else if(value.length < 2) {
                        return 'Name must be a minimum 2 character';
                      }else{
                        return null;
                      }
                  },
                  onSaved: (value) {

                  },
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: "Enter your Phone",
                  ),
                  validator : (value) {
                      if(value.length != 11){
                         return 'Phone must be 11 character';
                      }else{
                        return null;
                      }
                  },
                  onSaved: (value) {

                  },
                ),
                TextFormField(
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  maxLength: 1000,
                  decoration:  InputDecoration(labelText: "Enter your opinion"),
                  validator : (value) {
                      if(value.length == 0){
                         return 'Opinion is Required';
                      }else if(value.length < 5) {
                        return 'Name must be a minimum 2 character';
                      }else{
                        return null;
                      }
                  },
                  onSaved: (value) {

                  },
                ),
                Center(
                  child: CircleAvatar(
                    radius: 80,
                    child: _pickedImage == null ? Text("picture") : null,
                    backgroundImage: _pickedImage != null ? FileImage(_pickedImage) : null,
                  ),
                ),
                const SizedBox(height: 10),
                RaisedButton(
                  onPressed: (){
                    _showPicturesOptionsDialog(context);
                  },
                  child: Text('Pick Image'),
                ),
                Row(
                  children: <Widget>[
                    RaisedButton(
                      onPressed: handleSubmit,
                      child: Text("submit"),
                      color: Colors.green,
                      ),
                    SizedBox(width: 5),
                    RaisedButton(
                      onPressed: handleReset,
                      child: Text("Reset"),
                      color: Colors.deepOrange
                      ),
                  ],
                )
              ],
            )
          ),
        ),
      ],
    );
  }
}


void _closeDialog(context) {
  Navigator.of(context).pop();
}


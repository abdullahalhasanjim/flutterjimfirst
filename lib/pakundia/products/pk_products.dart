import 'package:flutter/material.dart';
import 'pk_product_details.dart';

class PkProducts extends StatelessWidget { 
  @override 
  Widget build (BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF21BFBD),
      body: ProductPage(),
    );
  }
}

class ProductPage extends StatefulWidget {
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  final List shopList = [
    { 'imageUrl' : 'assets/plate1.png'},
    { 'imageUrl' : 'assets/plate2.png'},
    { 'imageUrl' : 'assets/plate3.png'},
    { 'imageUrl' : 'assets/plate4.png'},
    { 'imageUrl' : 'assets/plate5.png'},
    { 'imageUrl' : 'assets/plate1.png'},
    { 'imageUrl' : 'assets/plate2.png'},
    { 'imageUrl' : 'assets/plate3.png'},
    { 'imageUrl' : 'assets/plate4.png'},
    { 'imageUrl' : 'assets/plate5.png'},
  ];
  @override
  Widget build(BuildContext context) {
    return  ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 15.0, left: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.arrow_back_ios),
                  color: Colors.white,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                Container(
                    width: 125.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.filter_list),
                          color: Colors.white,
                          onPressed: () {},
                        ),
                        IconButton(
                          icon: Icon(Icons.menu),
                          color: Colors.white,
                          onPressed: () {},
                        )
                      ],
                    ))
              ],
            ),
          ),
          SizedBox(height: 25.0),
          Padding(
            padding: EdgeInsets.only(left: 40.0),
            child: Row( 
              children: <Widget>[
                Text('টিয়া পাখি ফুড বেকারি ',
                    style: TextStyle(
                        fontFamily: 'Montserrat', 
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 25.0)),
              ],
            ),
          ),
          SizedBox(height: 40.0),
          Container(
            height: MediaQuery.of(context).size.height - 185.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(75.0)),
            ),
            child: ListView(
              primary: false,
              padding: EdgeInsets.only(left: 25.0, right: 20.0),
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 45.0),
                    child: Container(
                        height: MediaQuery.of(context).size.height - 300.0,
                        child: ListView(children: [
                          _buildFoodItem(context,'assets/plate1.png', 'Salmon bowl', '\$24.00'),
                          _buildFoodItem(context,'assets/plate2.png', 'Spring bowl', '\$22.00'),
                          _buildFoodItem(context,'assets/plate6.png', 'Avocado bowl', '\$26.00'),
                          _buildFoodItem(context,'assets/plate5.png', 'Berry bowl', '\$24.00'),
                          _buildFoodItem(context,'assets/plate3.png', 'Salmon bowl', '\$24.00'),
                          _buildFoodItem(context,'assets/plate4.png', 'Spring bowl', '\$22.00'),
              
                        ]))),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 3),
                    child: Container(
                      height: 80,
                      child : ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: shopList.length,
                      itemBuilder: (BuildContext context,int index) {
                        return Container(
                                height: 30,
                                width: 65,
                                child: GestureDetector(
                                  onTap : () {},
                                  child : Card (
                                    elevation: 10,
                                    clipBehavior: Clip.antiAliasWithSaveLayer,
                                    child: Padding(padding: EdgeInsets.all(1),child: Image.asset(shopList[index]['imageUrl'],fit: BoxFit.fill,),),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(7)
                                    ),
                                    )
                                ),
                              );
                      }),
                    )
                  )
              ],
            ),
          ),
        ],
    );
  }
}


Widget _buildFoodItem(BuildContext context, String imgPath, String foodName, String price) {
    return Padding(
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => PkProductDetails(heroTag: imgPath, foodName: foodName, foodPrice: price)
            ));
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Row(
                  children: [
                    Hero(
                      tag: imgPath,
                      child: Image(
                        image: AssetImage(imgPath),
                        fit: BoxFit.cover,
                        height: 75.0,
                        width: 75.0
                      )
                    ),
                    SizedBox(width: 10.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children:[
                        Text(
                          foodName,
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold
                          )
                        ),
                        Text(
                          price,
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontSize: 15.0,
                            color: Colors.grey
                          )
                        )
                      ]
                    )
                  ]
                )
              ),
              IconButton(
                icon: Icon(Icons.arrow_right),
                color: Colors.black,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => PkProductDetails(heroTag: imgPath, foodName: foodName, foodPrice: price)
            ));
                }
              )
            ],
          )
        ));
  }

import 'package:flutter/material.dart';
import 'end_drawer.dart';
import 'package:slimy_card/slimy_card.dart';
import 'package:flip_card/flip_card.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(" আমাদের সম্পর্কে ")),
      endDrawer: EndDrawer(),
      // body: AboutUsListView(),
      // body: AboutUsSiply(),
      // body : AboutUsList(),
      // body : AboutUsListModify(),
      body: AboutUsListTeam(),
    );
  }
}

class AboutUsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(
        crossAxisCount: 2,
        children: List.generate(20, (index) {
          return Center(
            child: GestureDetector(
              child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: Image.network(
                  'https://challengepost-s3-challengepost.netdna-ssl.com/photos/production/software_photos/000/545/093/datas/original.jpg',
                  fit: BoxFit.fill,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}

class AboutUsListModify extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(
        crossAxisCount: 2,
        children: List.generate(20, (index) {
          return Center(
            child: GestureDetector(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: Container(
                  height: 290,
                  //  child: AboutUsSiplyModify(),
                  child: AboutUsCard(),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}

class AboutUsListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.all(3),
        child: Container(
          child: SingleChildScrollView(
              child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(3),
                child: Row(
                  children: <Widget>[
                    Container(
                        width: 175,
                        height: 140,
                        child: GestureDetector(
                          onTap: () {
                            print('This is the tap of Blood');
                          },
                          child: Card(
                            semanticContainer: true,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Image.network(
                              'https://challengepost-s3-challengepost.netdna-ssl.com/photos/production/software_photos/000/545/093/datas/original.jpg',
                              fit: BoxFit.fill,
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            elevation: 5,
                            margin: EdgeInsets.all(10),
                          ),
                        )),
                  ],
                ),
              )
            ],
          )),
        ),
      ),
    );
  }
}

class AboutUsSiply extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        SlimyCard(
          color: Colors.red,
          width: 200,
          topCardHeight: 155,
          bottomCardHeight: 100,
          borderRadius: 15,
          topCardWidget: Center(
            child: Container(
              child: Image.network(
                  'https://challengepost-s3-challengepost.netdna-ssl.com/photos/production/software_photos/000/545/093/datas/original.jpg'),
            ),
          ),
          bottomCardWidget: Center(
            child: Text(
              'Hi this is jim',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 10, color: Colors.blue),
            ),
          ),
          slimeEnabled: true,
        ),
      ],
    );
  }
}

class AboutUsSiplyModify extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SlimyCard(
      color: Colors.red,
      width: 200,
      topCardHeight: 151,
      bottomCardHeight: 100,
      borderRadius: 15,
      topCardWidget: Center(
        child: Container(
          child: Image.network(
              'https://challengepost-s3-challengepost.netdna-ssl.com/photos/production/software_photos/000/545/093/datas/original.jpg'),
        ),
      ),
      bottomCardWidget: Center(
        child: Text(
          'Hi this is jim',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 10, color: Colors.blue),
        ),
      ),
      slimeEnabled: true,
    );
  }
}

class AboutUsCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          AspectRatio(
            aspectRatio: 18.0 / 11.0,
            child: Image.asset('assets/jim.png'),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Mr Jim',
                    style:
                        TextStyle(fontSize: 10, fontWeight: FontWeight.bold)),
                SizedBox(height: 8.0),
                Text('Founder, Software Engineer',
                    style: TextStyle(fontSize: 8, fontWeight: FontWeight.bold)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class AboutUsListTeam extends StatelessWidget {
  final List<dynamic> teams = [
    {'name' : "jim", 'image' : 'assets/Friends/jim.jpg'},
    {'name' : "sagor", 'image' : 'assets/Friends/sagor.jpg'},
    {'name' : "Tanvir", 'image' : 'assets/Friends/tanvir.jpg'},
    {'name' : "kawser", 'image' : 'assets/Friends/kawser.jpg'},
    {'name' : "shovon", 'image' : 'assets/Friends/shovon.jpg'},
    {'name' : "robin", 'image' : 'assets/Friends/robin.jpg'},
    {'name' : "shovo", 'image' : 'assets/Friends/shovo.jpg'},
    {'name' : "samrat", 'image' : 'assets/Friends/samrat.jpg'},
    {'name' : "priti", 'image' : 'assets/Friends/sadia.jpg'},
    {'name' : "priti2", 'image' : 'assets/Friends/tasnimpriti.jpg'},
    {'name' : "saifulvai", 'image' : 'assets/Friends/saifulvai.jpg'},
    {'name' : "sumonvai", 'image' : 'assets/Friends/sumonvai.jpg'},
    {'name' : "nayim", 'image' : 'assets/Friends/nayim.jpg'},
    {'name' : "nasir", 'image' : 'assets/Friends/nasir.jpg'},
    ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(
        crossAxisCount: 2,
        children: List.generate(teams.length, (index) {
          return Center(
            child: GestureDetector(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: FlipCard(
                  direction: FlipDirection.HORIZONTAL,
                  front: FrontCard(imageUrl: teams[index]['image']),
                  back: BackCard(
                      name: teams[index]['name'], color: Colors.deepOrange),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}

class FrontCard extends StatelessWidget {
  final String imageUrl;
  const FrontCard({Key key, this.imageUrl}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
          elevation: 10,
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: Image.asset(imageUrl,
            fit: BoxFit.fill,
            height: 250,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        );
  }
}

class BackCard extends StatelessWidget {
  final String name;
  final MaterialColor color;
  const BackCard({Key key, this.name, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      elevation: 10,
      child: Center(
        child: Text(
          name,
          style: TextStyle(
              color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
// import 'dart:convert';
import 'signup.dart';
import 'forgetpassword.dart';
import 'main.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:validators/validators.dart';
// import 'package:http/http.dart' as http;

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/signup': (BuildContext context) => new SignupPage(),
        '/forgot-password': (BuildContext context) => new ForgetPassword(),
      },
      home: new Login(),
    );
  }
}

class Login extends StatefulWidget {
  @override
  _LoginState createState() => new _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController mobileController = new TextEditingController();
  TextEditingController passController = new TextEditingController();

  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  SharedPreferences sharedPreferences;

  signIn(String mobile, String pass) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("token", "heythisisthetoken");
    if (mobile == '01680304447' && pass == 'rimjim') {
      sharedPreferences.setString("token", "heythisisthetoken");
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => MyHomePage()),
          (Route<dynamic> route) => false);
    }

    /* For logi API */
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // Map data = {
    //   'email': email,
    //   'password': pass
    // };
    // var jsonResponse = null;
    // var response = await http.post("YOUR_BASE_URL", body: data);
    // if(response.statusCode == 200) {
    //   jsonResponse = json.decode(response.body);
    //   if(jsonResponse != null) {
    //     setState(() {
    //       _isLoading = false;
    //     });
    //     sharedPreferences.setString("token", jsonResponse['token']);
    //     Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePage()), (Route<dynamic> route) => false);
    //   }
    // }
    // else {
    //   setState(() {
    //     _isLoading = false;
    //   });
    // print(response.body);
  }

  String validateMobile(value) {
    Pattern pattern = r'^(01)[3456789]{1}(\d){8}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Please enter a valid mobile number';
    else
      return null;
  }

  String validatePassword(value) {
    if (value.length < 6 || value.length > 18) {
      return 'Password should be 6 to 18 character';
    } else {
      return null;
    }
  }

  /* ENd For logi API */

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(15.0, 110.0, 0.0, 0.0),
                    child: Text('Hello',
                        style: TextStyle(
                            fontSize: 80.0, fontWeight: FontWeight.bold)),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(16.0, 185.0, 0.0, 0.0),
                    child: Text('Pakundia',
                        style: TextStyle(
                            fontSize: 60.0, fontWeight: FontWeight.bold)),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(266.0, 175.0, 0.0, 0.0),
                    child: Text(',',
                        style: TextStyle(
                            fontSize: 80.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.green)),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
              child: new Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: formUI(),
              ),
            ),
            SizedBox(height: 15.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'New to Pakundia ?',
                  style: TextStyle(fontFamily: 'Montserrat'),
                ),
                SizedBox(width: 5.0),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed('/signup');
                  },
                  child: Text(
                    'Register',
                    style: TextStyle(
                        color: Colors.green,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline),
                  ),
                )
              ],
            )
          ],
        ));
  }

  Widget formUI() {
    return new Column(
      children: <Widget>[
        TextFormField(
          controller: mobileController,
          keyboardType: TextInputType.phone,
          decoration: InputDecoration(
              labelText: 'MOBILE',
              labelStyle: TextStyle(
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                  color: Colors.grey),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.green))),
          validator: validateMobile,
        ),
        SizedBox(height: 20.0),
        TextFormField(
          controller: passController,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
              labelText: 'PASSWORD',
              labelStyle: TextStyle(
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                  color: Colors.grey),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.green))),
          obscureText: true,
          validator: validatePassword,
        ),
        SizedBox(height: 5.0),
        Container(
          alignment: Alignment(1.0, 0.0),
          padding: EdgeInsets.only(top: 15.0, left: 20.0),
          child: InkWell(
            onTap: () {
              Navigator.of(context).pushNamed('/forgot-password');
            },
            child: Text(
              'Forgot Password',
              style: TextStyle(
                  color: Colors.green,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Montserrat',
                  decoration: TextDecoration.underline),
            ),
          ),
        ),
        SizedBox(height: 40.0),
        Container(
          height: 40.0,
          child: Material(
            borderRadius: BorderRadius.circular(20.0),
            shadowColor: Colors.greenAccent,
            color: Colors.green,
            elevation: 7.0,
            child: GestureDetector(
              onTap: () {
                if (_formKey.currentState.validate()) {
                  // Scaffold.of(context).showSnackBar(SnackBar(content: Text('Logginng in ...')));
                  signIn(mobileController.text, passController.text);
                }
              },
              child: Center(
                child: Text(
                  'LOGIN',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Montserrat'),
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: 20.0),
        // Container(
        //   height: 40.0,
        //   color: Colors.transparent,
        //   child: Container(
        //     decoration: BoxDecoration(
        //         border: Border.all(
        //             color: Colors.black,
        //             style: BorderStyle.solid,
        //             width: 1.0),
        //         color: Colors.transparent,
        //         borderRadius: BorderRadius.circular(20.0)),
        //     child: Row(
        //       mainAxisAlignment: MainAxisAlignment.center,
        //       children: <Widget>[
        //         Center(
        //           child:
        //               ImageIcon(AssetImage('assets/facebook.png')),
        //         ),
        //         SizedBox(width: 10.0),
        //         Center(
        //           child: Text('Log in with facebook',
        //               style: TextStyle(
        //                   fontWeight: FontWeight.bold,
        //                   fontFamily: 'Montserrat')),
        //         )
        //       ],
        //     ),
        //   ),
        // )
      ],
    );
  }
}
